import React, { useState } from "react";

function AttendConferenceForm({ getAttendees, conferences }) {
  const [conference, setConference] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name,
      email,
      conference,
    };

    const attendeeUrl = "http://localhost:8001/api/attendees/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const attendeeResponse = await fetch(attendeeUrl, fetchOptions);
    if (attendeeResponse.ok) {
      setConference("");
      setName("");
      setEmail("");
      getAttendees();
      setHasSignedUp(true);
    }
  }

  return (
    <div className="my-5">
      <div className="row">
        <div className="col col-sm-auto">
          <img
            src="/logo.svg"
            alt="logo"
            width="300"
            className="bg-white rounded shadow d-block mx-auto mb-4"
          />
        </div>

        <div className="col-6">
          <div className="card shadow">
            <div className="card-body">
              <form
                className={hasSignedUp ? "d-none" : ""}
                onSubmit={handleSubmit}
                id="create-attendee-form"
              >
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference you'd like to attend.
                </p>
                <div
                  className={
                    conferences.length === 0
                      ? "d-flex justify-content-center mb-3"
                      : "d-none"
                  }
                  id="loading-conference-spinner"
                >
                  <div className="spinner-border text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <select
                  onChange={handleConferenceChange}
                  value={conference}
                  required
                  name="conference"
                  id="conference"
                  className={
                    conferences.length === 0 ? "d-none" : "form-select mb-3"
                  }
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => {
                    return (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
                <p>Now, tell us about yourself.</p>
                <div className="d-flex justify-content-between">
                  <div className="form-floating mb-3" style={{ width: "48%" }}>
                    <input
                      onChange={handleNameChange}
                      value={name}
                      placeholder="Name"
                      required
                      type="text"
                      name="name"
                      id="name"
                      className="form-control"
                    />
                    <label htmlFor="name">Your full name</label>
                  </div>
                  <div className="form-floating mb-3" style={{ width: "48%" }}>
                    <input
                      onChange={handleEmailChange}
                      value={email}
                      placeholder="Email"
                      required
                      type="email"
                      name="email"
                      id="email"
                      className="form-control"
                    />
                    <label htmlFor="email">Your email address</label>
                  </div>
                </div>
                <button className="btn btn-primary">I'm going!</button>
              </form>
              <div
                className={
                  hasSignedUp ? "`alert alert-success mb-0 p-3" : "d-none"
                }
                id="success-message"
              >
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendConferenceForm;
