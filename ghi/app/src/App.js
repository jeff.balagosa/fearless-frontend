import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import PresentationForm from "./PresentationForm";
import AttendConferenceForm from "./AttendConferenceForm";
import MainPage from "./MainPage";
import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  const [attendees, setAttendees] = useState([]);
  const [conferences, setConferences] = useState([]);

  async function getAttendees() {
    const url = "http://localhost:8001/api/attendees/";
    const response = await fetch(url);
    if (response.ok) {
      const { attendees } = await response.json();
      setAttendees(attendees);
    } else {
      console.error("An error occurred fetching the data");
    }
  }

  async function getConeferences() {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const { conferences } = await response.json();
      setConferences(conferences);
    } else {
      console.error("An error occurred fetching the data");
    }
  }

  useEffect(() => {
    getAttendees();
    getConeferences();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage conferences={conferences} />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route
              path="new"
              element={<ConferenceForm getConeferences={getConeferences} />}
            />
          </Route>
          <Route path="presentations">
            <Route
              path="new"
              element={<PresentationForm conferences={conferences} />}
            />
          </Route>
          <Route path="attendees">
            <Route
              path="new"
              element={
                <AttendConferenceForm
                  conferences={conferences}
                  getAttendees={getAttendees}
                />
              }
            />
            <Route
              path="list"
              element={<AttendeesList attendees={attendees} />}
            />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
